const input = document.createElement('input')
input.type = 'number'
input.style.width = '300px'
input.style.height = '100px'
input.style.fontSize = '50px'
input.style.border = '8px solid grey'

document.body.append(input)

const span = document.createElement('span')
span.textContent = 'Price:'
span.style.fontSize = '50px'

document.body.prepend(span)

input.addEventListener('focus', function() {
    input.style.color = 'green'
    input.style.borderColor = 'green'
    input.style.outline = 'none'
})

input.addEventListener('blur', function() {
    input.style.color = 'black'
    input.style.borderColor = 'grey'
    input.style.outline = 'none'

    getValue()
})

function getValue() {
    const value = Number(input.value) // +input.value

    if (value <= 0 || value == '') {
        getError()
    } else {
        const resultSpan = document.createElement('span')
        resultSpan.textContent = `Input value is ${value}`
        input.style.color = 'grey'
        resultSpan.style.fontSize = '50px'
        resultSpan.className = 'span_value'
        resultSpan.style.border = '4px solid orange'
        resultSpan.style.display = 'block'
        resultSpan.style.width = 'fit-content'
        resultSpan.style.margin = '10px'
        resultSpan.style.padding = '4px'
        document.body.append(resultSpan)
        buttonFunc()
    }
}

function getError() {
    input.style.borderColor = 'red'
    input.style.color = 'red'

    const textError = document.createElement('span')
    textError.className = 'myError'
    textError.textContent = 'Please, enter a correct value'
    textError.style.color = 'white'
    textError.style.padding = '10px'
    textError.style.backgroundColor = 'red'
    textError.style.fontSize = '50px'
    textError.style.display = 'block'
    
    document.body.append(textError)
    // input.onfocus = () => {
    //     input.style.borderColor = 'grey'
    //     input.style.color = 'grey'
    // }
}

function buttonFunc() {
    const myButton = document.createElement('button')
    myButton.style.width = '160px'
    myButton.style.height = '80px'
    myButton.textContent = 'Delete'
    document.body.append(myButton)

    myButton.addEventListener('click', removeData)
}

function removeData() {
    const span_value = document.querySelector('.span_value')
    const buttonDel = document.querySelector('button')

    // for (let i = 0; i < span_value.length; i++) {
    //     span_value[i].remove
        
    // }

    span_value.remove()
    buttonDel.remove()
    input.value = ''
}